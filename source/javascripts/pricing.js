$(function() {
  $('.experiment-control .plans').slick({
    appendDots: '.experiment-control .pricing-nav-mobile',
    dots: false,
    centerMode: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          dots: true,
          initialSlide: 1,
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true
        }
      }
    ]
  });

  if ($('.experiment-test').length) {
    $('.experiment-test .plans').slick({
      appendDots: '.experiment-test .pricing-nav-mobile',
      dots: false,
      centerMode: false,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            dots: true,
            initialSlide: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true
          }
        }
      ]
    });
  };
});
